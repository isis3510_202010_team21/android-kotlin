package com.augniture.beta.dependencyinjection

import android.app.Application
import com.augniture.beta.data.repository.CarritoRepository
import com.augniture.beta.data.repository.FavoritoRepository
import com.augniture.beta.data.repository.ProductoRepository
import com.augniture.beta.data.repository.UsuarioRepository
import com.augniture.beta.domain.interactors.favorito.GetFavoritos
import com.augniture.beta.domain.interactors.producto.*
import com.augniture.beta.domain.interactors.productocompra.AddOrdenCompra
import com.augniture.beta.domain.interactors.productocompra.AddProductoCompra
import com.augniture.beta.domain.interactors.productocompra.DeleteProductoCompra
import com.augniture.beta.domain.interactors.productocompra.GetProductosCompra
import com.augniture.beta.domain.interactors.usuario.AddUsuario
import com.augniture.beta.domain.interactors.usuario.GetCurrentUsuario
import com.augniture.beta.domain.interactors.usuario.RegisterUsuario
import com.augniture.beta.domain.interactors.usuario.SignInUsuario
import com.augniture.beta.framework.datasourceimpl.CarritoDataSourceImpl
import com.augniture.beta.framework.datasourceimpl.FavoritoDataSourceImpl
import com.augniture.beta.framework.datasourceimpl.ProductoDataSourceImpl
import com.augniture.beta.framework.datasourceimpl.UsuarioDataSourceImpl

class AugnitureApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val productoRepository = ProductoRepository(ProductoDataSourceImpl(this))

        val usuarioRepository = UsuarioRepository(UsuarioDataSourceImpl(this))

        val favoritoRepository = FavoritoRepository(FavoritoDataSourceImpl(this))

        val carritoRepository = CarritoRepository(CarritoDataSourceImpl(this))

        AugnitureViewModelFactory.inject(
            this,
            Interactors(
                SignInUsuario(usuarioRepository),
                RegisterUsuario(usuarioRepository),
                GetCurrentUsuario(usuarioRepository),
                AddUsuario(usuarioRepository),

                GetProductos(productoRepository),
                AddProducto(productoRepository),

                GetFavoritos(favoritoRepository),
                AddFavorito(favoritoRepository),
                DeleteFavorito(favoritoRepository),

                GetProductosCompra(carritoRepository),
                AddProductoCompra(carritoRepository),
                DeleteProductoCompra(carritoRepository),
                AddOrdenCompra(carritoRepository)
            )
        )
    }

}
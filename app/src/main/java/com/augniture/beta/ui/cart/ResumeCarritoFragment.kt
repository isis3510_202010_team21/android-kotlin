package com.augniture.beta.ui.home.featuredproducts

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Producto
import com.augniture.beta.ui.cart.ResumeCarritoAdapter
import com.augniture.beta.ui.general.detalleproducto.DetalleProductoFragment
import com.augniture.beta.ui.general.productos.CarritoViewModel
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.supportutilities.ItemClickListener
import com.augniture.beta.ui.supportutilities.SharedPreferencesConstants
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_resume_cart_products_list.*
import kotlinx.android.synthetic.main.fragment_resume_cart_products_list.view.*
import kotlinx.android.synthetic.main.resume_cart_product_cardview_item_single.*

class ResumeCarritoFragment() : Fragment(), ItemClickListener<Producto> {

    private lateinit var productosViewModel: ProductosViewModel

    private lateinit var carritoViewModel: CarritoViewModel

    private lateinit var resumenProductosCompraRecyclerView: RecyclerView

    private lateinit var resumenProductosCompraAdapter: ResumeCarritoAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val resumenCarritoFragmentView = inflater.inflate(R.layout.fragment_resume_cart_products_list, container, false)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and UserImage)
        val upperSubContainer: ViewGroup? = requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewSearchUpperTopAppBar: View = inflater.inflate(R.layout.back_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewSearchUpperTopAppBar)

        val backTopAppBarTitleTxt: TextView = requireActivity().findViewById(R.id.backTopAppBarTitleTxt)
        backTopAppBarTitleTxt.text = "Resumen de compra"

        // Setup lower container for the TopAppBar
        val lowerSubContainer: ViewGroup? = requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        lowerSubContainer?.removeAllViews()

        // INIT ATTRIBUTES
        productosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(ProductosViewModel::class.java)
        carritoViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(CarritoViewModel::class.java)

        resumenProductosCompraRecyclerView = resumenCarritoFragmentView.findViewById(R.id.resumeCartProductsRecyclerViewList)
        resumenProductosCompraAdapter =
            ResumeCarritoAdapter(
                itemRecyclerViewResourceId = R.layout.resume_cart_product_cardview_item_single,
                glide = Glide.with(this),
                itemClickListener = this
            )
        resumenProductosCompraRecyclerView.adapter = resumenProductosCompraAdapter

        return resumenCarritoFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        carritoViewModel.productosCompra.observe(viewLifecycleOwner, Observer {
            val productosCompra = it

            if (productosCompra.isNotEmpty()) {
                goToPaymentButton.visibility = View.VISIBLE

                var totalResumenCarrito: Double = 0.0

                for (productoCompra in productosCompra) {
                    totalResumenCarrito += (productoCompra.cantidad!! * productoCompra.producto?.precio!!)
                }

                resumeCartTotalCV.text = totalResumenCarrito.toString()
            }
            else {
                goToPaymentButton.visibility = View.GONE
            }

            resumenProductosCompraAdapter.update(productosCompra)
        })
        carritoViewModel.loadProductosCompra()

        goToPaymentButton.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(R.id.contentMain, MetodoPagoCarritoFragment())
            transaction.addToBackStack(null)
            transaction.commit()
        }

    }

    override fun onItemClicked(t: Producto) {
        productosViewModel.setProductoSeleccionado(t)

        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, DetalleProductoFragment(t))
        transaction.addToBackStack(null)
        transaction.commit()
    }

}

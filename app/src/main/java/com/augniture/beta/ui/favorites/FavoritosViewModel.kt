package com.augniture.beta.ui.favorites

import android.app.Application
import com.augniture.beta.dependencyinjection.AugnitureViewModel
import com.augniture.beta.dependencyinjection.Interactors
import com.augniture.beta.domain.Favorito
import com.augniture.beta.domain.Producto
import com.augniture.beta.domain.Usuario
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavoritosViewModel(application: Application, interactors: Interactors) : AugnitureViewModel(application, interactors) {

    fun addFavorito(usuario: Usuario, producto: Producto) {
        GlobalScope.launch(Dispatchers.Default) {
            interactors.addFavorito(usuario, producto)
        }
    }

    fun deleteFavorito(usuario: Usuario, favorito: Favorito) {
        GlobalScope.launch(Dispatchers.Default) {
            interactors.deleteFavorito(usuario, favorito)
        }
    }

}
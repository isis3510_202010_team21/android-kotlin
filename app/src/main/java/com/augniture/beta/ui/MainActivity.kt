package com.augniture.beta.ui

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Usuario
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.home.HomeFragment
import com.augniture.beta.ui.home.featuredproducts.CarritoFragment
import com.augniture.beta.ui.search.FavoritosFragment
import com.augniture.beta.ui.search.BusquedaFragment
import com.augniture.beta.ui.supportutilities.SharedPreferencesConstants
import com.google.android.material.bottomappbar.BottomAppBar

class MainActivity : AppCompatActivity() {

    private lateinit var optionsMenu: Menu

    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var productosViewModel: ProductosViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i("MAINACT", "Llego Main Act")

        sharedPreferences = getSharedPreferences(SharedPreferencesConstants.SP_SESION_USUARIO, Context.MODE_PRIVATE)

        productosViewModel = ViewModelProviders.of(this, AugnitureViewModelFactory).get(ProductosViewModel::class.java)

        window.statusBarColor = Color.WHITE
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        distributeIcons()
        setupARButton()
    }

    override fun onStart() {
        super.onStart()

        // Load favorites
        var usuarioActual = Usuario.USUARIO_VACIO
        usuarioActual.id = sharedPreferences.getString(SharedPreferencesConstants.KEY_USUARIO_ACTUAL_ID, "")

        if (usuarioActual.id != "") { productosViewModel.loadProductosFavoritos(usuarioActual) }

        // Initial call to start HomeFragment
        initHomeFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bottom_nav_menu, menu)
        optionsMenu = menu

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        changeColors(item!!.itemId)
        var fragment: Fragment? = null

        when (item.itemId) {
            R.id.navigation_home -> { fragment = HomeFragment() }
            R.id.navigation_search -> { fragment = BusquedaFragment() }
            R.id.navigation_favourites -> { fragment = FavoritosFragment() }
            R.id.navigation_shopping_cart -> { fragment = CarritoFragment() }
            /*
            R.id.navigation_shopping_cart ->
                fragment = CartFragment()
            */
        }

        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.contentMain, fragment)
            transaction.commit()
        }

        return false
    }

    private fun initHomeFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, HomeFragment())
        transaction.commit()
    }

    private fun distributeIcons() {
        val bottomAppBar: BottomAppBar = findViewById(R.id.bottomAppBar)
        setSupportActionBar(bottomAppBar)

        if (bottomAppBar.childCount > 0) {
            val actionMenuView =
                bottomAppBar.getChildAt(0) as androidx.appcompat.widget.ActionMenuView
            actionMenuView.layoutParams.width =
                androidx.appcompat.widget.ActionMenuView.LayoutParams.MATCH_PARENT
        }
    }

    private fun setupARButton() {
        /*
        arBtn.setOnClickListener { view ->
            changeColors(view.id)
            val fragment = AugmentedFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.contentMain, fragment)
            transaction.commit()
        }
        */
    }

    private fun changeColors(id: Int) {
        validateFab(id)

        validateHome(id, optionsMenu)
        validateSearch(id, optionsMenu)
        validateFavourites(id, optionsMenu)
        validateCart(id, optionsMenu)
    }

    private fun validateFab(id: Int) {
        val fab: View = findViewById(R.id.arBtn)

        if (id == R.id.arBtn) {
            fab.setBackgroundResource(R.drawable.ic_photo)
        } else {
            fab.setBackgroundResource(R.drawable.ic_photo_no_selected)
        }
    }

    private fun validateHome(id: Int, menu: Menu) {
        val item = menu.findItem(R.id.navigation_home)
        if (id == R.id.navigation_home) {
            item.setIcon(R.drawable.ic_home)
        } else {
            item.setIcon(R.drawable.ic_home_no_selected)
        }
    }

    private fun validateSearch(id: Int, menu: Menu) {
        val item = menu.findItem(R.id.navigation_search)
        if (id == R.id.navigation_search) {
            item.setIcon(R.drawable.ic_search)
        } else {
            item.setIcon(R.drawable.ic_search_no_selected)
        }
    }

    private fun validateFavourites(id: Int, menu: Menu) {
        val item = menu.findItem(R.id.navigation_favourites)
        if (id == R.id.navigation_favourites) {
            item.setIcon(R.drawable.ic_favourites)
        } else {
            item.setIcon(R.drawable.ic_favourites_no_selected)
        }
    }

    private fun validateCart(id: Int, menu: Menu) {
        val item = menu.findItem(R.id.navigation_shopping_cart)
        if (id == R.id.navigation_shopping_cart) {
            item.setIcon(R.drawable.ic_shopping_cart)
        } else {
            item.setIcon(R.drawable.ic_shopping_cart_no_selected)
        }
    }
}

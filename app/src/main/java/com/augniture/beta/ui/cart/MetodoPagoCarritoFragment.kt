package com.augniture.beta.ui.home.featuredproducts

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Usuario
import com.augniture.beta.framework.network.NetworkManager
import com.augniture.beta.ui.SuccessActivity
import com.augniture.beta.ui.general.productos.CarritoViewModel
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.supportutilities.SharedPreferencesConstants
import kotlinx.android.synthetic.main.fragment_payment_method.*

class MetodoPagoCarritoFragment() : Fragment() {

    private lateinit var productosViewModel: ProductosViewModel

    private lateinit var carritoViewModel: CarritoViewModel

    private lateinit var sharedPreferences: SharedPreferences

    private var isOnline: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val metodoPagoCarritoFragmentView = inflater.inflate(R.layout.fragment_payment_method, container, false)

        // Setup SharedPreferences
        sharedPreferences = requireActivity().getSharedPreferences(SharedPreferencesConstants.SP_SESION_USUARIO, Context.MODE_PRIVATE)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and UserImage)
        val upperSubContainer: ViewGroup? = requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewSearchUpperTopAppBar: View = inflater.inflate(R.layout.back_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewSearchUpperTopAppBar)

        val backTopAppBarTitleTxt: TextView = requireActivity().findViewById(R.id.backTopAppBarTitleTxt)
        backTopAppBarTitleTxt.text = "Metodo de pago"

        // Setup lower container for the TopAppBar
        val lowerSubContainer: ViewGroup? = requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        lowerSubContainer?.removeAllViews()

        // INIT ATTRIBUTES
        productosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(ProductosViewModel::class.java)
        carritoViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(CarritoViewModel::class.java)

        return metodoPagoCarritoFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

            completePaymentButton.setOnClickListener {
                isOnline = NetworkManager.isOnline(requireActivity())

                if (isOnline) {
                    var usuarioActual = Usuario.USUARIO_VACIO
                    usuarioActual.id = sharedPreferences.getString(
                        SharedPreferencesConstants.KEY_USUARIO_ACTUAL_ID,
                        SharedPreferencesConstants.VALOR_DEF_STRING
                    )

                    carritoViewModel.addProductosOrden(usuarioActual)

                    // Go to success activity
                    val successActivity = Intent(requireActivity(), SuccessActivity::class.java)
                    successActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    successActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    successActivity.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    successActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                    startActivity(successActivity)

                    requireActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
                else {
                    // Go to error handling activity
                }

            }


    }

}

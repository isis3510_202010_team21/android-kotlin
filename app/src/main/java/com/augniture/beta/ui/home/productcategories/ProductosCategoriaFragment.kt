
package com.augniture.beta.ui.home.productcategories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Producto
import com.augniture.beta.ui.general.detalleproducto.DetalleProductoFragment
import com.augniture.beta.ui.general.productos.ProductosAdapter
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.supportutilities.ItemClickListener
import com.bumptech.glide.Glide

class ProductosCategoriaFragment: Fragment(), ItemClickListener<Producto> {

    private lateinit var categoriasProductosViewModel: CategoriasProductosViewModel

    private lateinit var productosViewModel: ProductosViewModel

    private lateinit var productosRecyclerView: RecyclerView

    private lateinit var productosAdapter: ProductosAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val productosCategoriaFragmentView = inflater.inflate(R.layout.fragment_category_products_list, container, false)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and BackImageView)
        val upperSubContainer: ViewGroup? = requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewHomeCategoryUpperTopAppBar: View = inflater.inflate(R.layout.back_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewHomeCategoryUpperTopAppBar)

        // Setup lower container for the TopAppBar
        val lowerSubContainer: ViewGroup? = requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        lowerSubContainer?.removeAllViews()

        // Setup back button onClick Listener
        val backBtnImageView: ImageView? = requireActivity().findViewById(R.id.backTopAppBarBackButtonImg)
        backBtnImageView?.setOnClickListener {

        }

        // INIT ATTRIBUTES
        productosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(
            ProductosViewModel::class.java)
        categoriasProductosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(CategoriasProductosViewModel::class.java)
        productosAdapter =
            ProductosAdapter(
                itemRecyclerViewResourceId = R.layout.product_cardview_item_single,
                glide = Glide.with(this),
                itemClickListener = this
            )
        productosRecyclerView = productosCategoriaFragmentView.findViewById(R.id.categoryProductsRecyclerViewList)
        productosRecyclerView.adapter = productosAdapter

        return productosCategoriaFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriasProductosViewModel.categoriaSeleccionada.observe(viewLifecycleOwner, Observer {
            val categoria = it

            val fragmentTitle: TextView? = requireActivity().findViewById(R.id.backTopAppBarTitleTxt)
            fragmentTitle?.text = "Categoria - ${categoria.nombre}"

            productosViewModel.loadProductosCategoria(categoria)
        })

        productosViewModel.productosCategoria.observe(viewLifecycleOwner, Observer {
            val productosCategoria = it

            productosAdapter.update(productosCategoria)
        })

    }

    override fun onItemClicked(t: Producto) {
        productosViewModel.setProductoSeleccionado(t)

        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, DetalleProductoFragment(t))
        transaction.addToBackStack(null)
        transaction.commit()
    }

}

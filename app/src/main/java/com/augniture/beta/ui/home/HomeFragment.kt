package com.augniture.beta.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.augniture.beta.R
import com.augniture.beta.ui.home.featuredproducts.ProductosDestacadosFragment
import com.augniture.beta.ui.home.productcategories.CategoriasProductosFragment
import com.augniture.beta.ui.supportutilities.SharedPreferencesConstants
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.tabs.TabLayout

class HomeFragment : Fragment() {

    private lateinit var homeFragmentStatePagerAdapter: HomeFragmentStatePagerAdapter

    private lateinit var viewPager: ViewPager

    private lateinit var tabLayout: TabLayout

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val homeFragmentView = inflater.inflate(R.layout.home_viewpager, container, false)

        sharedPreferences = requireActivity().getSharedPreferences(SharedPreferencesConstants.SP_SESION_USUARIO, Context.MODE_PRIVATE)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and UserImage)
        val upperSubContainer: ViewGroup? =
            requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewHomeUpperTopAppBar: View =
            inflater.inflate(R.layout.base_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewHomeUpperTopAppBar)

        val topAppBarUserImage: ImageView? =
            requireActivity().findViewById(R.id.baseTopAppBarUserImg)
        topAppBarUserImage?.visibility = ImageView.VISIBLE
        val glideInstance = Glide.with(this)
        val photoUrl = sharedPreferences.getString(SharedPreferencesConstants.KEY_USUARIO_ACTUAL_FOTO, "")

        if (topAppBarUserImage != null) {
            glideInstance
                .load(photoUrl)
                .error(glideInstance.load(R.drawable.ic_profile_icon))
                .apply(RequestOptions.circleCropTransform())
                .into(topAppBarUserImage)
        }

        topAppBarUserImage?.setOnClickListener{
            TODO("Not yet implemented")
        }

        val topAppBarTitleTxt: TextView? = requireActivity().findViewById(R.id.baseTopAppBarTitleTxt)
        topAppBarTitleTxt?.text = "Inicio"

        // Setup lower container for the TopAppBar (TabLayout)
        val lowerSubContainer: ViewGroup? =
            requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        val viewHomeLowerTopAppBar: View =
            inflater.inflate(R.layout.home_lower_top_app_bar, lowerSubContainer, false)
        lowerSubContainer?.removeAllViews()
        lowerSubContainer?.addView(viewHomeLowerTopAppBar)

        // INTI ATTRIBUTES
        homeFragmentStatePagerAdapter =
            HomeFragmentStatePagerAdapter(fragmentManager = childFragmentManager)

        /*
        val bundleConstantsMap = FragmentBundleConstants().bundleConstantsMap

        val productosFragment = FragmentArgsGenerator.createFragmentWithArgs(
            ProductosFragment(
                R.layout.fragment_home_products_list,
                R.id.productsRecyclerViewList
            ),
            FragmentBundleConstants.FROM_HOME_TO_PRODUCTS_KEY,
            bundleConstantsMap[FragmentBundleConstants.FROM_HOME_TO_PRODUCTS_KEY]
        )
        */

        homeFragmentStatePagerAdapter.addFragment(ProductosDestacadosFragment(), "Destacados")
        homeFragmentStatePagerAdapter.addFragment(CategoriasProductosFragment(), "Categorias")

        viewPager = homeFragmentView.findViewById(R.id.homeViewPager)
        viewPager.adapter = homeFragmentStatePagerAdapter

        tabLayout = requireActivity().findViewById(R.id.homeTabLayout)
        tabLayout.setupWithViewPager(viewPager)

        return homeFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDetach() {
        //invalidateBanner()
        super.onDetach()
    }

    /*
    private fun invalidateBanner() {
        val layoutButtons: View? = requireActivity().findViewById(R.id.constraint_toggle)
        layoutButtons?.visibility = View.GONE

        val card: View? = requireActivity().findViewById(R.id.title_card)

        val layoutSearch: View? = requireActivity().findViewById(R.id.constraint_search)
        layoutSearch?.visibility = View.GONE

        val layoutPrice: View? = requireActivity().findViewById(R.id.constraint_cart)
        layoutPrice?.visibility = View.GONE

        card?.invalidate()
    }
    */
}

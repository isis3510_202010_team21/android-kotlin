package com.augniture.beta.ui.augmented

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.augniture.beta.R

class AugmentedFragment : Fragment() {

    private lateinit var augmentedViewModel: AugmentedViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        augmentedViewModel =
            ViewModelProviders.of(this).get(AugmentedViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_cart, container, false)


        //invalidateBanner()


        val textView: TextView? = activity?.findViewById(R.id.baseTopAppBarTitleTxt)
        textView?.setText(R.string.title_augmented)

        return root
    }

    /*
    private fun invalidateBanner() {
        val layoutButtons: View? = activity?.findViewById(R.id.constraint_toggle)
        layoutButtons?.visibility = View.GONE
        val card: View? = activity?.findViewById(R.id.title_card)
        val layoutSearch: View? = activity?.findViewById(R.id.constraint_search)
        layoutSearch?.visibility = View.GONE
        val layoutPrice: View? = activity?.findViewById(R.id.constraint_cart)
        layoutPrice?.visibility = View.GONE
        card?.invalidate()
    }

     */

}
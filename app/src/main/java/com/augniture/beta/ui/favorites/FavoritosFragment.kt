package com.augniture.beta.ui.search

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Producto
import com.augniture.beta.domain.Usuario
import com.augniture.beta.ui.general.detalleproducto.DetalleProductoFragment
import com.augniture.beta.ui.general.productos.ProductosAdapter
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.supportutilities.ItemClickListener
import com.augniture.beta.ui.supportutilities.SharedPreferencesConstants
import com.bumptech.glide.Glide
import com.google.firebase.firestore.DocumentReference
import kotlinx.coroutines.tasks.await

class FavoritosFragment : Fragment(), ItemClickListener<Producto> {

    private lateinit var productosViewModel: ProductosViewModel

    private lateinit var productsRecyclerView: RecyclerView

    private lateinit var productosAdapter: ProductosAdapter

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val productosFavoritosFragmentView = inflater.inflate(R.layout.fragment_favorite_products_grid, container, false)

        // Init SharedPreferences
        sharedPreferences = requireActivity().getSharedPreferences(SharedPreferencesConstants.SP_SESION_USUARIO, Context.MODE_PRIVATE)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and UserImage)
        val upperSubContainer: ViewGroup? = requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewSearchUpperTopAppBar: View = inflater.inflate(R.layout.base_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewSearchUpperTopAppBar)

        val baseTopAppBarTitleTxt: TextView = requireActivity().findViewById(R.id.baseTopAppBarTitleTxt)
        baseTopAppBarTitleTxt.text = "Favoritos"

        // Setup lower container for the TopAppBar
        val lowerSubContainer: ViewGroup? = requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        lowerSubContainer?.removeAllViews()

        // INIT ATTRIBUTES
        productosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(
            ProductosViewModel::class.java)
        productsRecyclerView = productosFavoritosFragmentView.findViewById(R.id.favoriteProductsRecyclerViewGrid)
        productsRecyclerView.layoutManager = GridLayoutManager(requireActivity(), 2)
        productosAdapter =
            ProductosAdapter(
                itemRecyclerViewResourceId = R.layout.product_cardview_item_grid,
                glide = Glide.with(this),
                itemClickListener = this
            )
        productsRecyclerView.adapter = productosAdapter

        return productosFavoritosFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val currentUsuarioId= sharedPreferences.getString(SharedPreferencesConstants.KEY_USUARIO_ACTUAL_ID, "")
        var usuarioActual = Usuario.USUARIO_VACIO
        usuarioActual.id = currentUsuarioId

        productosViewModel.productosFavoritos.observe(viewLifecycleOwner, Observer { itList ->
            val favoritos = itList

            val productosFavoritos= favoritos.map {
                val productoFavorito = it.referenciaProducto

                Producto(
                    productoFavorito?.id,
                    productoFavorito?.nombre,
                    productoFavorito?.imagen,
                    productoFavorito?.destacado,
                    productoFavorito?.descripcion,
                    productoFavorito?.categoria,
                    productoFavorito?.precio,
                    productoFavorito?.modeloar
                )
            }

            productosAdapter.update(productosFavoritos)
        })
        productosViewModel.loadProductosFavoritos(usuarioActual)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onItemClicked(t: Producto) {
        productosViewModel.setProductoSeleccionado(t)

        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, DetalleProductoFragment(t))
        transaction.addToBackStack(null)
        transaction.commit()
    }

}
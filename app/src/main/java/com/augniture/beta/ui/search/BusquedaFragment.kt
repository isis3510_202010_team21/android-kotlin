package com.augniture.beta.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.SearchView.OnQueryTextListener
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplitude.api.Amplitude
import com.amplitude.api.AmplitudeClient
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.domain.Producto
import com.augniture.beta.ui.general.detalleproducto.DetalleProductoFragment
import com.augniture.beta.ui.general.productos.ProductosAdapter
import com.augniture.beta.ui.general.productos.ProductosViewModel
import com.augniture.beta.ui.supportutilities.ItemClickListener
import com.bumptech.glide.Glide
import org.json.JSONObject

class BusquedaFragment : Fragment(), ItemClickListener<Producto> {

    private lateinit var productosViewModel: ProductosViewModel

    private lateinit var productosRecyclerView: RecyclerView

    private lateinit var productosAdapter: ProductosAdapter

    private lateinit var amplitudeInstance: AmplitudeClient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val productosBusquedaFragmentView = inflater.inflate(R.layout.fragment_search_products_grid, container, false)

        amplitudeInstance = Amplitude
            .getInstance()
            .initialize(requireActivity(), getString(R.string.amplitude_ak))
            .enableForegroundTracking(requireActivity().application)

        // SETUP TOP APP BAR
        // Setup upper container for the TopAppBar (Title TextView and UserImage)
        val upperSubContainer: ViewGroup? = requireActivity().findViewById(R.id.upperTopAppBarContainer)
        val viewSearchUpperTopAppBar: View = inflater.inflate(R.layout.base_upper_top_app_bar, upperSubContainer, false)
        upperSubContainer?.removeAllViews()
        upperSubContainer?.addView(viewSearchUpperTopAppBar)

        val baseTopAppBarTitleTxt: TextView = requireActivity().findViewById(R.id.baseTopAppBarTitleTxt)
        baseTopAppBarTitleTxt.text = "Buscar"

        // Setup lower container for the TopAppBar
        val lowerSubContainer: ViewGroup? = requireActivity().findViewById(R.id.lowerTopAppBarContainer)
        val viewSearchLowerTopAppBar: View = inflater.inflate(R.layout.search_lower_top_app_bar, upperSubContainer, false)
        lowerSubContainer?.removeAllViews()
        lowerSubContainer?.addView(viewSearchLowerTopAppBar)

        val searchView: SearchView = requireActivity().findViewById(R.id.searchSearchView)
        searchView.setOnClickListener {
            searchView.onActionViewExpanded()
        }

        // INIT ATTRIBUTES
        productosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(
            ProductosViewModel::class.java)
        productosRecyclerView = productosBusquedaFragmentView.findViewById(R.id.productsRecyclerViewGrid)
        productosRecyclerView.layoutManager = GridLayoutManager(requireActivity(), 2)
        productosAdapter =
            ProductosAdapter(
                itemRecyclerViewResourceId = R.layout.product_cardview_item_grid,
                glide = Glide.with(this),
                itemClickListener = this
            )
        productosRecyclerView.adapter = productosAdapter

        return productosBusquedaFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productosViewModel.productosBusqueda.observe(viewLifecycleOwner, Observer { itList ->
            val productos = itList

            productosAdapter.update(productos)
        })
        productosViewModel.loadProductosBusqueda("")

        val searchSearchView : SearchView = requireActivity().findViewById(R.id.searchSearchView)
        searchSearchView.setOnQueryTextListener(object : OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean { return false }

            override fun onQueryTextChange(newText: String): Boolean {
                productosViewModel.loadProductosBusqueda(newText)

                return true
            }

        })
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onItemClicked(t: Producto) {
        productosViewModel.setProductoSeleccionado(t)

        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, DetalleProductoFragment(t))
        transaction.addToBackStack(null)
        transaction.commit()
    }

}
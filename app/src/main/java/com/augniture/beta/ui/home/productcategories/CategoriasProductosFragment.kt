package com.augniture.beta.ui.home.productcategories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.amplitude.api.Amplitude
import com.amplitude.api.AmplitudeClient
import com.augniture.beta.R
import com.augniture.beta.dependencyinjection.AugnitureViewModelFactory
import com.augniture.beta.ui.home.featuredproducts.CategoriasProductosAdapter
import com.augniture.beta.ui.supportutilities.ItemClickListener
import com.bumptech.glide.Glide
import org.json.JSONObject

class CategoriasProductosFragment : Fragment(),
    ItemClickListener<CategoriasProductosViewModel.Categoria> {

    private lateinit var categoriasProductosViewModel: CategoriasProductosViewModel

    private lateinit var productCategoriesRecyclerView: RecyclerView

    private lateinit var categoriasProductosAdapter: CategoriasProductosAdapter

    private lateinit var amplitudeInstance: AmplitudeClient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // INFLATE VIEW
        val categoriasProductosFragmentView = inflater.inflate(R.layout.fragment_home_product_categories, container, false)

        amplitudeInstance = Amplitude
            .getInstance()
            .initialize(requireActivity(), getString(R.string.amplitude_ak))
            .enableForegroundTracking(requireActivity().application)

        // INIT ATTRIBUTES
        categoriasProductosViewModel = ViewModelProviders.of(requireActivity(), AugnitureViewModelFactory).get(CategoriasProductosViewModel::class.java)
        categoriasProductosAdapter = CategoriasProductosAdapter(glide = Glide.with(this), itemClickListener = this)
        productCategoriesRecyclerView = categoriasProductosFragmentView.findViewById(R.id.productCategoriesRecyclerView)
        productCategoriesRecyclerView.adapter = categoriasProductosAdapter

        return categoriasProductosFragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriasProductosViewModel.categorias.observe(viewLifecycleOwner, Observer {
            val categoria = it

            categoriasProductosAdapter.update(categoria)
        })
        categoriasProductosViewModel.loadCategorias()
    }

    override fun onItemClicked(t: CategoriasProductosViewModel.Categoria) {
        categoriasProductosViewModel.setCategoriaSeleccionada(t)

        val data = JSONObject(hashMapOf(
            "searchTerm" to t.nombre,
            "origin" to "Categoria"
        ))

        amplitudeInstance.logEvent("search", data)

        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentMain, ProductosCategoriaFragment())
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}

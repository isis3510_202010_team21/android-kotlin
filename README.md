# Augniture

**Augniture** es un proyecto académico enmarcado en el proceso del curso
[Construcción de Aplicaciones Móviles](https://cursos.virtual.uniandes.edu.co/isis3510/),
impartido en el programa de pre-grado de Ingeniería de Sistemas y Computación 
de la [Universidad de los Andes](https://uniandes.edu.co/).

**Augniture** pretende conectar a los pequeños y medianos vendedores de 
artículos decorativos, artesanías y recursos afines al diseño de interiores con
el consumidor final, en busca de 

### Wiki

La documentación relacionada con el proceso de desarrollo del
proyecto puede ser encontrada en [aquí](https://gitlab.com/isis3510_202010_team21/wiki/-/wikis/home).